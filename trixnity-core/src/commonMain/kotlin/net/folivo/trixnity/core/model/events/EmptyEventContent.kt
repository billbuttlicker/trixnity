package net.folivo.trixnity.core.model.events

import kotlinx.serialization.Serializable

@Serializable
object EmptyEventContent : EventContent